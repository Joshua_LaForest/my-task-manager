import fastapi
from fastapi import (
    Depends,
    HTTPException,
    status,
    Response,
    APIRouter,
    Request,
)
from queries.tasks import TaskIn, TaskOut, TaskQueries, PatchTaskAvailable
from typing import List, Optional
from pydantic import BaseModel

router = APIRouter()

@router.post("/create-task", response_model=TaskOut)
async def taskIn(
    task: TaskIn,
    queries: TaskQueries = Depends()
):
    return queries.taskIn(task)

@router.get("/get-all-tasks", response_model=list[TaskOut])
async def tasksOut(
    tasks: TaskQueries = Depends()
):
    return tasks.tasksOut()

@router.get("/get-task/{task_id}", response_model=Optional[TaskOut])
async def taskOut(
    task_id: int,
    queries: TaskQueries = Depends()
) -> TaskOut:
    task = queries.taskOut(task_id)
    return task

@router.patch("/task/{task_id}/patch", response_model=TaskOut)
async def taskPatch(
    task_id: int,
    task: PatchTaskAvailable,
    queries: TaskQueries = Depends()
) -> TaskOut:
    return queries.taskPatch(
        task_id,
        PatchTaskAvailable(
            task = task.task,
            notes = task.notes,
            due = task.due,
            complete=task.complete
        )
    )


@router.delete("/task/{task_id}/delete", response_model=bool)
async def taskDelete(
    task_id: int,
    queries: TaskQueries = Depends(),
) -> bool:
    return queries.taskDelete(task_id)