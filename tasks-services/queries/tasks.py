from pydantic import BaseModel
from typing import Optional
from queries.pool import pool
from datetime import datetime, time, timedelta
from typing import Annotated
from fastapi import Body

class TaskIn(BaseModel):
    task: str
    notes: str
    due: datetime
    complete: bool
    


class TaskOut(BaseModel):
    id: int
    task: str
    notes: str
    due: datetime
    complete: bool
    
    
class PatchTaskAvailable(BaseModel):
    task: str | None = None
    notes: str | None = None
    due: datetime | None = None
    complete: bool | None = None
    


class TaskQueries:
    def taskIn(self, task: TaskIn) -> TaskOut:
        with pool.connection() as con:
            with con.cursor() as cur:
                result = cur.execute(
                    """
                    INSERT INTO tasks(
                        task,
                        notes,
                        due,
                        complete
                    )
                    VALUES (%s, %s, %s, %s)
                    RETURNING id
                    """,
                    [
                        task.task,
                        task.notes,
                        task.due,
                        task.complete
                    ],
                )
                id = result.fetchone()[0]
                return self.task_in_to_out(id, task)
    
    
    def tasksOut(self) -> list[TaskOut]:
        with pool.connection() as con:
            with con.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT 
                    id,
                    task,
                    notes,
                    due,
                    complete
                    FROM tasks
                    ORDER BY id;
                    """
                )
                return [self.record_to_task_out(record) for record in result]
    
    def taskOut(
        self, task_id: int
    ) -> Optional[TaskOut]:
        with pool.connection() as con:
            with con.cursor() as cur:
                result = cur.execute(
                    """
                    SELECT
                    id,
                    task,
                    notes,
                    due,
                    complete
                    FROM tasks
                    WHERE id = %s
                    """,
                    [task_id]
                )
                record = result.fetchone()
                if record is None:
                    return None
                return self.record_to_task_out(record)
    
    
    def taskPatch(
        self, task_id: int, task: PatchTaskAvailable
    ) -> TaskOut:
        with pool.connection() as con:
            with con.cursor() as cur:
                lst = [
                    item[0]
                    for item in dict(task).items()
                    if item[1] is not None
                ]
                columns = " = %s, ".join(lst) + " = %s"
                lst_params = [
                        item
                        for item in dict(task).values()
                        if item is not None
                    ]
                lst_params.append(task_id)
                cur.execute(
                    f"""
                    UPDATE tasks
                    SET {columns}
                    WHERE id = %s
                    """,
                    lst_params,
                )
                con.commit()
                return self.taskOut(task_id)
    
    def taskDelete(self, task_id: int) -> bool:
        with pool.connection() as con:
            with con.cursor() as cur:
                cur.execute(
                    """
                    DELETE FROM tasks
                    WHERE id = %s
                    """,
                    [task_id],
                )
                return True
    
    
    def task_in_to_out(self, id: int, task: TaskIn):
        old_data = task.dict()
        return TaskOut(id=id, **old_data)
    
    def record_to_task_out(self, record):
        return TaskOut(
            id=record[0],
            task=record[1],
            notes=record[2],
            due=record[3],
            complete=record[4]
        )