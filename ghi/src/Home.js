import { useNavigate } from "react-router-dom";
import moment from 'moment-timezone';
import "./Home.css"

function Home(props){
    let navigate = useNavigate();
    
    const date = new Date()
    const formated = new Intl.DateTimeFormat('nl-BE')
    const today = (formated.format(date)).toString()
    const Todaysplit = today.split("/").reverse()
    if(Todaysplit[1]<10){
        Todaysplit[1] = "0"+Todaysplit[1]
    }
    if(Todaysplit[2]<10){
        Todaysplit[2] = "0"+Todaysplit[2]
    }

    const Today = Todaysplit.join("-")

    const handleComplete = (taskId) =>{
        fetch(`${process.env.REACT_APP_TASKS_API}/task/${taskId}/patch`, {
            method: "PATCH",
            headers:{
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ complete: true }),
        }).then(() => {
            window.location.reload(true);
        })
    }



    return(
        <>
        <div className="div-header">
            <div>
                <p className="pageIndicator"> 
                    This Will Display all the remaining tasks for today!!!
                </p>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Task</th>
                        <th>Description</th>
                        <th>Due</th>
                    </tr>
                </thead>
                <tbody>
                    {props.get_tasks.filter(task => task.complete ===false
                    && task.due.toString().startsWith(Today.toString())).map((task) =>(
                        <tr key={task.id}>
                            <td>{task.task}</td>
                            <td>{task.notes}</td>
                            <td>{moment.tz(task.due, 'UTC').tz('America/Anchorage').format('MM/DD/YYYY')}</td>
                            <td>
                                <button
                                className='complete-btn'
                                onClick={() => handleComplete(task.id)}
                                id="complete"
                                type="button"
                                >Complete</button>
                            </td>
                            <td>
                                <button
                                className='edit-btn'
                                onClick={()=>{navigate(`/task/${task.id}/patch`); 
                                props.setTaskId(task.id)}}
                                id="edit"
                                type="button"
                                >Edit</button>
                            </td>
                        </tr>
                    )
                    )}
                </tbody>
            </table>
        </div>
        </>
        );
    };
export default Home;