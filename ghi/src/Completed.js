import "./Completed.css"

export default function Completed(props){
    const handleDelete = (taskId) =>{
        fetch(
            `${process.env.REACT_APP_TASKS_API}/task/${taskId}/delete`,
        {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
            }
        ).then(() => {
                window.location.reload();
        });
    }
    return (
        <>
        <div className="div-header">
            <div>
                <p className="pageIndicator"> 
                    These Are All The Tasks You've Completed
                </p>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Task</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    {props.get_tasks.filter(task => task.complete ===true).map((task) =>(
                        <tr key={task.id}>
                            <td>{task.task}</td>
                            <td>{task.notes}</td>
                            <td><button
                            className='delete-btn'
                            onClick={() => handleDelete(task.id)}
                            id="delete"
                            type="button">Delete</button></td>
                        </tr>
                    )
                    )}
                </tbody>
            </table>
        </div>
        </>
    )
}