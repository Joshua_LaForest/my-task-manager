import './App.css';
import {BrowserRouter, NavLink, Routes, Route } from 'react-router-dom';
import Home from './Home.js'
import { useEffect, useState } from "react";
import CreateTask from "./taskCreate.js";
import Completed from "./Completed";
// @ts-ignore
import FutureTasks from "./FutureTasks";
import TaskUpdate from './UpdateTask';


function App() {
  const[get_tasks, getTasks] = useState([]);
  const[task_id, setTaskId] = useState([]);
  useEffect(()=>{
      async function GetTasks(){
      let url = `${process.env.REACT_APP_TASKS_API}/get-all-tasks`;
      let response = await fetch(url);
      let data = await response.json();
      if(response.ok){
          getTasks(data)
      }
  }
  GetTasks();
  }, []
  
  )
  return (
    <div>
      <div className="div-header">
        <header className="header-main">
          <h1 className='h1thing'>
            My Task Manager!
          </h1>
        </header>
      </div>
      <BrowserRouter>
      <div className="nav-top">
        <ul className="nav-list">
          <li><NavLink className="href" to="/" >Home</NavLink></li>
          <li><NavLink className="href" to="/new-task">Create a New Task</NavLink></li>
          <li><NavLink className="href" to="/completed-tasks">Completed Tasks</NavLink></li>
          <li><NavLink className="href" to="/upcoming-tasks">Upcoming Tasks</NavLink></li>
        </ul>
      </div>
      <div className='body-main'>
          <Routes>
            <Route path="/" element={<Home get_tasks={get_tasks} task_id ={task_id} setTaskId = {setTaskId}/>} />
            <Route path="new-task" element={<CreateTask get_tasks={get_tasks}/>} />
            <Route path="completed-tasks" element={<Completed get_tasks={get_tasks}/>} />
            <Route path="upcoming-tasks" element={<FutureTasks get_tasks={get_tasks} task_id ={task_id} setTaskId = {setTaskId}/>} />
            <Route path={`/task/:ID/patch`} element={<TaskUpdate task_id={task_id} get_tasks={get_tasks}/>} />

          </Routes>
        </div>
    
    </BrowserRouter>
    <div className='footer'> 
    </div>
    </div>
  );
}

export default App;

