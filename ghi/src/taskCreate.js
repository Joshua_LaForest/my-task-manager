import { useState } from "react";
import Datetime from 'react-datetime';
import "./taskCreate.css";


function CreateTask() {
    const [task, setTask] = useState("");
    const [notes, setNotes] = useState("");
    const [due, setDue] = useState("");
    const [complete] = useState(false)
    const [isCalendarOpen, setIsCalendarOpen] = useState(false);

    const handleSubmit = (e) => {
        e.preventDefault();

        const data = {
            task,
            notes,
            due,
            complete,
        };
        fetch(`${process.env.REACT_APP_TASKS_API}/create-task`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json",
            },
            body: JSON.stringify(data),

        }
        ).then(()=> window.location.reload())
    }
    
    const handleDateChange = date => {
        setDue(date.toISOString());
    }
    const handleCalendarOpen = () => {
        setIsCalendarOpen(true);
    }

    const handleCalendarClose = () => {
        setIsCalendarOpen(false);
    }

    const calendarStyle = {
        position: "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        backgroundColor: "black",
        color: "white",
        padding: "20px",
        borderRadius: "10px",
        cursor: "pointer"
    }

    return(
        <>
        <div className="div-header">
            <div>
                <p className="pageIndicator"> 
                    Would You Like to Create a New Task?
                </p>
            </div>
            <form className="FormSubmit" onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="task" className="form-label">
                        Task
                    </label>
                </div>
                <input
                    value={task}
                    onChange={(t)=> setTask(t.target.value)}
                    type="text"
                    id="task"
                    placeholder="Task"
                />
                <div className="mb-3">
                    <label htmlFor="notes" className="form-label">
                        Notes
                    </label>
                </div>
                <textarea
                    value={notes}
                    onChange={(n)=> setNotes(n.target.value)}
                    type="text"
                    id="notes"
                    placeholder="Notes"
                />
                <div className="mb-3">
                    <label htmlFor="due" className="form-label">
                        Due on
                    </label>
                </div>
                
                {!isCalendarOpen && (
                    <button className="c-btn" onClick={handleCalendarOpen}>Open Calendar</button>
                    )}
                {isCalendarOpen && (
                    <div style={calendarStyle}>
                        <Datetime onChange={handleDateChange} className="mb-3"/>
                        <button className="c-btn" onClick={handleCalendarClose}>Close Calendar</button>
                    </div>
                )}
            <div className="mb-3">
            <button className="create-btn" onClick={() => handleSubmit}>Submit</button>
            </div>
            
            </form>
        </div>
        </>
    );
};
export default CreateTask;