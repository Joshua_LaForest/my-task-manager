import moment from 'moment-timezone';
import "./Home.css"
import { useNavigate } from "react-router-dom";

export default function FutureTasks(props){
    const handleComplete = (taskId) =>{
        fetch(`${process.env.REACT_APP_TASKS_API}/task/${taskId}/patch`, {
            method: "PATCH",
            headers:{
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ complete: true }),
        }).then(() => {
            window.location.reload(true);
        })
    }

    let navigate = useNavigate();

    return(
        <>
        <div className="div-header">
            <div>
                <p className="pageIndicator"> 
                    These are all incomplete tasks!!!
                </p>
            </div>
            <table>
                <thead>
                    <tr>
                        <th>Task</th>
                        <th>Description</th>
                        <th>Due</th>
                    </tr>
                </thead>
                <tbody>
                    {props.get_tasks.filter(task => task.complete ===false)
                    .sort((a, b) => moment(a.due).diff(moment(b.due))).map((task) =>(
                        <tr key={task.id}>
                            <td>{task.task}</td>
                            <td>{task.notes}</td>
                            <td>{moment.tz(task.due, 'UTC').tz('America/Anchorage').format('MM/DD/YYYY')}</td>
                            <td>
                                <button
                                className='complete-btn'
                                onClick={() => handleComplete(task.id)}
                                id="complete"
                                type="button"
                                >Complete</button>
                            </td>
                            <td>
                                <button
                                className='edit-btn'
                                onClick={()=>{navigate(`/task/${task.id}/patch`); 
                                props.setTaskId(task.id)}}
                                id="edit"
                                type="button"
                                >Edit</button>
                            </td>
                        </tr>
                    )
                    )}
                </tbody>
            </table>
        </div>
        </>
        );
    };
